import pytest
import requests
from random import randint
from endpoints import all_cities, city_by_id

from faker import Faker

fake = Faker()

@pytest.fixture
def new_city():
    random_name = fake.uuid4()
    random_population = randint(1, 10000)
    body = {
        "name": random_name,
        "population": random_population
    }
    response_create = requests.post(all_cities, json=body)
    assert response_create.status_code == 201
    return random_name

@pytest.fixture
def second_body():
    random_new_name = fake.uuid4()
    random_new_population = randint(1, 10000)
    edit_body = {
        'name': random_new_name,
        'population': random_new_population
    }
    return edit_body


def test_get_city(new_city):
    response = requests.get(all_cities)
    assert response.status_code == 200


def test_get_city_by_id(new_city):
    response_dict = requests.get(all_cities)
    last_id = response_dict.json()[0]['id']
    response_get = requests.get(city_by_id(last_id))
    assert response_get.status_code == 200


def test_create(new_city):
    response = requests.get(all_cities)
    response_dict = response.json()
    city_list = [a['name'] for a in response_dict]
    assert new_city in city_list


def test_delete_city(new_city):
    response = requests.get(all_cities)
    response_dict = response.json()
    last_id = response_dict[0]['id']
    response_delete = requests.delete(city_by_id(last_id))
    assert response_delete.status_code == 204


def test_edit_city(new_city, second_body):
    response = requests.get(all_cities)
    response_dict = response.json()
    last_id = response_dict[0]['id']
    put_response = requests.put(city_by_id(last_id), json=second_body)
    assert put_response.status_code == 200



